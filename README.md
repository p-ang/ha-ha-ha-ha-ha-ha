# 哈哈哈哈哈哈

#### 项目简介及其用法

![项目代码](https://images.gitee.com/uploads/images/2021/1121/194727_23a579c9_9720012.png "屏幕截图.png")
应用c/c++实现加减乘除的简易计算
#### 例程运行及其相关结果

![两个数相加](https://images.gitee.com/uploads/images/2021/1121/194840_029b486b_9720012.png "屏幕截图.png")
![两个数相减](https://images.gitee.com/uploads/images/2021/1121/194929_55415a45_9720012.png "屏幕截图.png")
![两个数相乘](https://images.gitee.com/uploads/images/2021/1121/195005_cbb4ccf9_9720012.png "屏幕截图.png")
![两个数相除](https://images.gitee.com/uploads/images/2021/1121/195037_21816735_9720012.png "屏幕截图.png")
![判断异常](https://images.gitee.com/uploads/images/2021/1121/195439_47785473_9720012.png "屏幕截图.png")